<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210827111909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rendezvous ADD deleted TINYINT(1) NOT NULL, ADD datefinrdv VARCHAR(255) NOT NULL, ADD datedebutrdv VARCHAR(255) NOT NULL, DROP date_debut_rdv, DROP date_fin_rdv');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rendezvous ADD date_debut_rdv DATETIME DEFAULT NULL, ADD date_fin_rdv DATETIME NOT NULL, DROP deleted, DROP datefinrdv, DROP datedebutrdv');
    }
}
