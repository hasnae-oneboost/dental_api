<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210825103027 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE rendezvous (id INT AUTO_INCREMENT NOT NULL, patient_id INT DEFAULT NULL, cabinet_id INT DEFAULT NULL, date_debut_rdv DATETIME DEFAULT NULL, date_fin_rdv DATETIME NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_C09A9BA86B899279 (patient_id), INDEX IDX_C09A9BA8D351EC (cabinet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rendezvous ADD CONSTRAINT FK_C09A9BA86B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE rendezvous ADD CONSTRAINT FK_C09A9BA8D351EC FOREIGN KEY (cabinet_id) REFERENCES cabinet (id)');
        $this->addSql('ALTER TABLE patient CHANGE datenaissance datenaissance DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE patient RENAME INDEX uniq_1adad7eb3d427250 TO UNIQ_1ADAD7EBABE530DA');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE rendezvous');
        $this->addSql('ALTER TABLE patient CHANGE datenaissance datenaissance DATE NOT NULL');
        $this->addSql('ALTER TABLE patient RENAME INDEX uniq_1adad7ebabe530da TO UNIQ_1ADAD7EB3D427250');
    }
}
